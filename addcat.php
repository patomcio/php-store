<?php
require_once 'connect.php';

function createCategory($name) {
  global $pdo;

  $stmt = $pdo->prepare("INSERT INTO product_category (name) VALUES (:name)");
  $stmt->bindParam(':name', $name);

  if ($stmt->execute()) {
    return true;
  } else {
    return false;
  }
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $name = $_POST['name'];

  if (createCategory($name)) {
    echo "<script>alert('Product created successfully');</script>";
    header('Location: index.php');
  } else {
    echo "<script>alert('Failed to create product.');</script>";
    header('Location: addcat.php');
  }
}
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Adicionar Categoria</title>

    <link rel="stylesheet" href="global.css">
  </head>
  <body>
    <form action="" method="POST">
      <div>
        <label for="name">Nome:</label>
        <input type="text" id="name" name="name" required><br>
      </div>

      <input class="btn" type="submit" value="Criar Categoria">
    </form>
  </body>
</html>
