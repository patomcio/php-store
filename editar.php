<?php
require_once 'connect.php';

function getAllCategories() {
  global $pdo;
  $stmt = $pdo->prepare("SELECT id, name FROM product_category");
  $stmt->execute();
  $categories = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $categories;
}

$id = $_GET['id'];

$stmt = $pdo->prepare("SELECT * FROM product WHERE id = :id LIMIT 1");
$stmt->bindParam(":id", $id);
$stmt->execute();

$res = $stmt->fetch(PDO::FETCH_ASSOC);

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $name = $_POST['name'];
  $price = $_POST['price'];
  $category_id = $_POST['category_id'];

  $stmt = $pdo->prepare("UPDATE product SET name = :name, price = :price, category_id = :category_id WHERE id = :id");
  $stmt->bindParam(':id', $id);
  $stmt->bindParam(':name', $name);
  $stmt->bindParam(':price', $price);
  $stmt->bindParam(':category_id', $category_id);
  $stmt->execute();

  header('Location: index.php');
}
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Home</title>

    <link rel="stylesheet" href="global.css">
  </head>
  <body>
    <form action="" method="POST">
      <div>
        <label for="name">Nome:</label>
        <input type="text" value="<?php echo $res['name'] ?>" id="name" name="name" required>

        <label for="price">Preço:</label>
        <input type="number" id="price" value="<?php echo $res['price'] ?>" name="price" step="0.01" min="0" required>

        <label for="category_id">Categoria:</label>
        <select id="category_id" name="category_id" required>
            <?php
            $categories = getAllCategories();

            foreach ($categories as $category) {
              if ($res['category_id'] === $category['id']) {
                echo "<option value='{$category['id']}' selected>{$category['name']}</option>";
              } else {
                echo "<option value='{$category['id']}'>{$category['name']}</option>";
              }
            }
            ?>
        </select>
      </div>

      <input class="btn" type="submit" value="Salvar">
    </form>
  </body>
</html>
