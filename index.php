<?php
require_once 'connect.php';

function getAllProductsWithCategories() {
  global $pdo;

  $stmt = $pdo->prepare("
    SELECT p.id, p.name, p.price, pc.name AS category_name
    FROM product p
    LEFT JOIN product_category pc ON p.category_id = pc.id
  ");

  $stmt->execute();

  $products = $stmt->fetchAll(PDO::FETCH_ASSOC);

  return $products;
}
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Home</title>

    <link rel="stylesheet" href="global.css">
  </head>
  <body>
    <div class="flex">
      <a class="btn" href="addprod.php">Adicionar Produto</a>
      <a class="btn" href="addcat.php">Adicionar Categoria</a>
    </div>

    <table>
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Price</th>
          <th>Category</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $products = getAllProductsWithCategories();

        foreach ($products as $product) {
          echo "<tr>";
          echo "<td>{$product['id']}</td>";
          echo "<td>{$product['name']}</td>";
          echo "<td>R\${$product['price']}</td>";
          echo "<td>{$product['category_name']}</td>";
          echo "<td>
                <a href='remover.php?id={$product['id']}'>Deletar</a>
                <a href='editar.php?id={$product['id']}'>Editar</a>
                </td>";
          echo "</tr>";
        }
       ?>
      </tbody>
    </table>
  </body>
</html>
