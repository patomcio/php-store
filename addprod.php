<?php
require_once 'connect.php';

function getAllCategories() {
  global $pdo;
  $stmt = $pdo->prepare("SELECT id, name FROM product_category");
  $stmt->execute();
  $categories = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $categories;
}

function createProduct($name, $price, $category_id) {
  global $pdo;

  $stmt = $pdo->prepare("INSERT INTO product (name, price, category_id) VALUES (:name, :price, :category_id)");
  $stmt->bindParam(':name', $name);
  $stmt->bindParam(':price', $price);
  $stmt->bindParam(':category_id', $category_id);

  if ($stmt->execute()) {
    return true;
  } else {
    return false;
  }
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $name = $_POST['name'];
  $price = $_POST['price'];
  $category_id = $_POST['category_id'];

  if (createProduct($name, $price, $category_id)) {
    echo "<script>alert('Product created successfully');</script>";
    header('Location: index.php');
  } else {
    echo "<script>alert('Failed to create product.');</script>";
    header('Location: addprod.php');
  }
}
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Adicionar Produto</title>

    <link rel="stylesheet" href="global.css">
  </head>
  <body>
    <form action="" method="POST">
      <div>
        <label for="name">Nome:</label>
        <input type="text" id="name" name="name" required>

        <label for="price">Preço:</label>
        <input type="number" id="price" name="price" step="0.01" min="0" required>

        <label for="category_id">Categoria:</label>
        <select id="category_id" name="category_id" required>
            <?php
            $categories = getAllCategories();

            foreach ($categories as $category) {
              echo "<option value='{$category['id']}'>{$category['name']}</option>";
            }
            ?>
        </select>
      </div>

      <input class="btn" type="submit" value="Criar Produto">
    </form>
  </body>
</html>
