<?php
require_once 'connect.php';

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
  $id = $_GET['id'];

  $stmt = $pdo->prepare("DELETE FROM product WHERE id = :id");
  $stmt->bindParam(':id', $id);
  $stmt->execute();

  header('Location: index.php');
}
